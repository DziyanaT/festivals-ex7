import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Editeur } from 'src/app/models/editeur';
import { Jeu } from 'src/app/models/jeu';
import { FestivalsService } from 'src/app/services/festivals.service';
import { Festival } from 'src/app/models/festival';

@Component({
  selector: 'app-editeur',
  templateUrl: './editeur.component.html',
  styleUrls: ['./editeur.component.css']
})
export class EditeurComponent implements OnInit, OnChanges {
  editor!: Editeur;
  existingEditors!: Editeur[];
  creatingNewEditor: boolean = false;
  editorGroup!: FormGroup;
  jeuToEdit!:Jeu;
  editorFestival!: Festival;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, private festivalsService: FestivalsService, private location: Location) {}

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('id')) {
      const id = this.route.snapshot.paramMap.get('id');
      if (id) {
        this.festivalsService.getEditor(id)
        .subscribe(editor => {
          if (editor) {
            this.editor = editor;
            this.festivalsService.getJeux(id)
            .subscribe(jeux => {
              if (jeux) {
                this.editor.jeux = jeux;
                console.log(this.editor);
              }
              this.editorGroup = this.fb.group({
                name: [this.editor.nomSociete, Validators.required],
                contacts: [this.editor.contacts]
              });
            })
          }
        })
      }
    }
    else {
      this.editor = new Editeur("");
      this.creatingNewEditor = true;
      this.editorGroup = this.fb.group({
        name: [this.editor.nomSociete, Validators.required],
        contacts: [this.editor.contacts]
      });
      if (this.route.snapshot.paramMap.has('festivalId')) {
        const festivalId = this.route.snapshot.paramMap.get('festivalId');
        console.log("festival id: " + festivalId);
        if (festivalId) {
          this.festivalsService.getFestival(festivalId)
          .subscribe(fest => {
            this.editorFestival = fest;
          });
          this.festivalsService.getAllEditors()
          .subscribe(ed => {
            this.existingEditors = ed;
          })
        }
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.editorGroup = this.fb.group({
      name: [this.editor.nomSociete, Validators.required],
      contacts: [this.editor.contacts]
    });
    console.log(this.editor);
  }

  onSubmit() {
    this.editor.nomSociete = this.editorGroup?.get("name")?.value;
    this.editor.contacts = this.editorGroup?.get("contacts")?.value;
    this.festivalsService.addUpdateEditor(this.editor, this.editorFestival);
    this.location.back();
  }

  editJeu(jeu: Jeu) {
    this.jeuToEdit = jeu;
  }

  submitEditedJeu(jeu: Jeu) {
    this.festivalsService.addUpdateJeu(jeu, this.editor.id || "");
  }

  deleteJeu(jeu: Jeu) {
    this.festivalsService.deleteJeu(jeu, this.editor.id || "");
  }

  addNewGame() {
    this.jeuToEdit = new Jeu("");
  }

  editEditor(editor: Editeur) {
    this.router.navigate(['editeur/' + editor?.id ]);
  }

  addToFestival(editor: Editeur) {
    if (editor.id) {
      this.editorFestival.editeurs.push("/editeurs/" + editor.id);
      this.festivalsService.addUpdateFestival(this.editorFestival);
      this.location.back();
    }
  }

  deleteEditor(editor: Editeur) {
    this.festivalsService.deleteEditor(editor);
  }
}

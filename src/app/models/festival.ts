import { Optional } from "@angular/core";
import { Editeur } from "./editeur";

export class Festival {
    static sqmTable = 6;

    public get tableTotal() : number { return this.tablemax_1 + this.tablemax_2 +
   this.tablemax_3; }

    constructor(
        public name: string,
        @Optional() public id?: string,
        @Optional() public tablemax_1: number = 35,
        @Optional() public tablemax_2: number = 35,
        @Optional() public tablemax_3: number = 35,
        @Optional() public tableprice_1: number = 64,
        @Optional() public tableprice_2: number = 72,
        @Optional() public tableprice_3: number = 28,
        @Optional() public sqmprice_1?: number,
        @Optional() public sqmprice_2?: number,
        @Optional() public sqmprice_3?: number,
        @Optional() public tablebooked_1: number = 20,
        @Optional() public tablebooked_2: number = 20,
        @Optional() public tablebooked_3: number = 20,
        @Optional() public editeurs: string[] = [],
        @Optional() public sqmbooked_1: number = 150,
        @Optional() public sqmbooked_2: number = 150,
        @Optional() public sqmbooked_3: number = 150,
        @Optional() public revenue: number = 1000,
        @Optional() public visitor: boolean = false,
        @Optional() public editeursObj?: Editeur[]
    ) {
        if (!this.sqmprice_1) {
            this.sqmprice_1 = this.tableprice_1 / 6;
        }
        if (!this.sqmprice_2) {
            this.sqmprice_2 = this.tableprice_2 / 6;
        }
        if (!this.sqmprice_3) {
            this.sqmprice_3 = this.tableprice_3 / 6;
        }
    }
   }
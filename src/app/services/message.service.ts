import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor() { }

  public messages: string[] = []

  log(message: string) {
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }
}

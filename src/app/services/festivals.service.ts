import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { FieldValue } from '@firebase/firestore';
import { forkJoin, map, Observable, take, tap } from 'rxjs';
import { Editeur } from '../models/editeur';
import { Festival } from '../models/festival';
import { Jeu } from '../models/jeu';
import { FestivaljsonService } from './festivaljson.service';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class FestivalsService {

  private path = '/festivals/';
  private pathEditeur = "/editeurs/"
  private festivalCollection!: AngularFirestoreCollection<Festival>;
  private editeurCollection!: AngularFirestoreCollection<Editeur>;
  private editeursPathsTemp: any;

  constructor(private festivalStore: AngularFirestore, private messageService: MessageService) {
    this.festivalCollection = festivalStore.collection(this.path);
    this.editeurCollection = festivalStore.collection(this.pathEditeur);
  }

  json2Festival(json: any): Festival {
    return new Festival(
      json.name, json.id, json.tablemax_1, json.tablemax_2,
      json.tablemax_3, json.tableprice_1, json.tableprice_2, json.tableprice_3,
      json.sqmprice_1, json.sqmprice_2, json.sqmprice_3, json.tablebooked_1, json.tablebooked_2, json.tablebooked_3, json.editeurs);
  }

  json2Editeur(json: any): Editeur {
    return new Editeur(json.nomSociete, json.id, json.contacts);
  }

  json2Jeu(json: any): Jeu {
    return new Jeu(json.nom, json.id, json.type, json.ageMin, json.ageMax, json.nbJoueursMin, json.nbJoueursMax, json.dureeMinutes);
  }

  getAllFestivals(): Observable<Festival[]> {
      return this.festivalCollection
      .valueChanges({ idField: "id" }).pipe(
        tap(doc=>{this.messageService.log(`doc=${JSON.stringify(doc)}`) }),
        map(data => data.map(doc => this.json2Festival(doc)))
      );
  }

  getFestival(id: String): Observable<Festival> {
    var itemDoc = this.festivalStore.doc<Festival>(this.path+ id);
    return itemDoc.valueChanges()
    .pipe(
       map( fest => {
        let festival = this.json2Festival(fest);
        this.editeursPathsTemp = fest?.editeurs;
        return festival
      })
    );
  }

  getEditors(editeursJson: any = this.editeursPathsTemp): Observable<Editeur[]> {
    let observables = []
    let itemDoc;
    for (let editeurRef of editeursJson) {
      itemDoc = this.festivalStore.doc<Editeur>(editeurRef);
      observables.push(itemDoc.valueChanges({ idField: "id" })
      .pipe(
         map( editeur => this.json2Editeur(editeur)),
         take(1)
      ));
    }
    return forkJoin(observables);
  }

  getEditor(editorId: String): Observable<Editeur> {
    var itemDoc = this.festivalStore.doc<Festival>(this.pathEditeur + editorId);
    return itemDoc.valueChanges({ idField: "id" })
    .pipe(
       map( ed => this.json2Editeur(ed))
    );
  }

  getAllEditors(): Observable<Editeur[]> {
    return this.editeurCollection
    .valueChanges({ idField: "id" }).pipe(
      map(data => data.map(doc => this.json2Editeur(doc)))
    );
  }

  addUpdateEditor(editor: Editeur, festival?: Festival) {
    if (editor.id == null) {
      editor.id = this.festivalStore.createId();
      if (festival) {
        festival.editeurs.push(this.pathEditeur + editor.id);
        delete festival.editeursObj;
        this.festivalCollection.doc(festival.id).set(Object.assign({}, festival));
      }
    }
    delete editor.jeux;
    this.editeurCollection.doc(editor.id).set(Object.assign({}, editor));
  }

  deleteEditorRef(editor: Editeur, festival: Festival) {
    for (let i = 0; i < festival.editeurs.length; i++) {
      if (festival.editeurs[i].includes(editor?.id || "-1")) {
        festival.editeurs.splice(i, 1);
      }
    }

    delete festival.editeursObj;
    this.festivalCollection.doc(festival.id).set(Object.assign({}, festival));
  }

  // Not to be used, we will only delete editor references from festival documents
  deleteEditor(editor: Editeur) {
    this.festivalStore.doc<Editeur>(this.pathEditeur+editor.id).delete();
  }

  getJeux(editorId: String) {
    let jeuxCollection = this.festivalStore.collection(this.pathEditeur + editorId + '/jeux/');
    return jeuxCollection
    .valueChanges({ idField: "id" }).pipe(
      map(data => data.map(doc => this.json2Jeu(doc)))
    );
  }

  addUpdateJeu(jeu: Jeu, editorId: string) {
    if (editorId == "") {
      return;
    }
    if (jeu.id == null) {
      jeu.id = this.festivalStore.createId();
    }
    let jeuxCollection = this.festivalStore.collection(this.pathEditeur + editorId + '/jeux/');
    jeuxCollection.doc(jeu.id).set(Object.assign({}, jeu));
  }

  deleteJeu(jeu: Jeu, editorId: string) {
    let jeuxCollection = this.festivalStore.collection(this.pathEditeur + editorId + '/jeux/');
    this.festivalStore.doc<Festival>(this.pathEditeur + editorId + '/jeux/' + jeu.id).delete();
  }

  deleteFestival(festival: Festival) {
    this.festivalStore.doc<Festival>(this.path+festival.id).delete();
  }

  addUpdateFestival(festival: Festival) {
    if (festival.id == null) {
      festival.id = this.festivalStore.createId();
    }
    delete festival.editeursObj;
    this.festivalCollection.doc(festival.id).set(Object.assign({}, festival));
  }

  addNewFestival(festival: Festival) {
    if (festival.id == null) {
      festival.id = this.festivalStore.createId()
    }
    this.festivalCollection.doc(festival.id).get()
    .subscribe(doc => {
      if (!doc.exists) {
        delete festival.editeursObj;
        this.festivalCollection.doc(festival.id).set(Object.assign({}, festival));
      } // else doc exists!
    });
  }
}
